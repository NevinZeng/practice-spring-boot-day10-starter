## O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

- Code Review and Show Case.
- Learn Spring Boot Mapper and practice extracting dto to specify transmission attributes.
- Learn the basics of Flyway and practice using Flyway.
- Learn Retro, including what Retro is, who needs Retro and its steps, and conduct a Retrospective Meeting within the group.
- Give a talk about Containers and learn about microservices and CI/ CDS shared by other groups.

## R (Reflective): Please use one word to express your feelings about today's class.

Light-hearted.

## I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

- In my opinion, group speech is a continuous learning process. Through the preparation of speech, group members can get to know the unfamiliar content, learn it, get familiar with it, and then share it in the class, so that they can be more familiar with this content.

## D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

- In the future, Retro can be conducted regularly to collect the confusion and opinions of members in the group, and the plan for the next cycle can be formulated by summarizing the learning of one period.
