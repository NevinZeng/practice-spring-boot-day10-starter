package com.afs.restapi.dto;

import com.afs.restapi.entity.Employee;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CompanyRequest {
    private String name;
    private List<Employee> employees;
}
