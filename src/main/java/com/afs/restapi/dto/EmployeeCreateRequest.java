package com.afs.restapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeCreateRequest {

    private String name;
    private Integer age;
    private String gender;
    private Integer salary;
    private Integer companyId;
    private Boolean status;
}
