package com.afs.restapi.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EmployeeUpdateRequest {
    private Integer age;
    private Integer salary;
}
