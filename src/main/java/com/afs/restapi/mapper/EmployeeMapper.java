package com.afs.restapi.mapper;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class EmployeeMapper {
    public static Employee toEmployeeEntity(EmployeeCreateRequest employeeCreateRequest) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeCreateRequest, employee);
        return employee;
    }

    public static List<EmployeeResponse> toEmployeeResponseList(List<Employee> employees) {
        return employees.stream().map(EmployeeMapper::toEmployeeResponse).collect(Collectors.toList());
    }

    public static EmployeeResponse toEmployeeResponse(Employee employee) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee, employeeResponse);
        return employeeResponse;
    }
}
