package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.afs.restapi.mapper.EmployeeMapper.*;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return toEmployeeResponseList(employeeRepository.findAllByStatusTrue());
    }

    public Employee update(int id, EmployeeUpdateRequest toUpdate) {
        Employee employee = findById(id);
        if (toUpdate.getAge() != null) {
            employee.setAge(toUpdate.getAge());
        }
        if (toUpdate.getSalary() != null) {
            employee.setSalary(toUpdate.getSalary());
        }
        employeeRepository.save(employee);
        return employee;
    }

    public Employee findById(int id) {
        return employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGenderAndStatusTrue(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return employeeRepository.findAllByStatusTrue(pageRequest).toList();
    }

    public Employee insert(EmployeeCreateRequest employeeCreateRequest) {
        return employeeRepository.save(toEmployeeEntity(employeeCreateRequest));
    }

    public void delete(int id) {
        Employee employee = findById(id);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
